﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using CPM_COST.Calculations;
using Microsoft.Msagl.Drawing;
using Microsoft.Msagl.GraphViewerGdi;
using Microsoft.Msagl.Layout.Layered;

namespace CPM_COST
{
    enum ActivityEnum
    {
        Name = 0,
        FollowingActivity = 1,
        NormalTime = 2,
        CriticalTime = 3,
        NormalCost = 4,
        CriticalCost = 5, 
        Description = 6

    }
    public partial class Form1 : Form
    {
        List<Activity> Activities { get; set; } = new List<Activity>();
        List<string> Names { get; set; } = new List<string>();
        List<string> Followings { get; set; } = new List<string>();

        Graph graph;
        GViewer gViewer;

        bool isGraphCreated = false;
        double totalCost = 0;

        public Form1()
        {
            InitializeComponent();

            gViewer = new GViewer() { Dock = DockStyle.Right, Width = dataGridView1.Width};
            SuspendLayout();
            Controls.Add(gViewer);
            ResumeLayout();
            graph = new Graph();
            var sugiyamaSettings = (SugiyamaLayoutSettings)graph.LayoutAlgorithmSettings;
            sugiyamaSettings.NodeSeparation *= 2;

            gViewer.Graph = graph;

            dataGridView1.Rows.Add();
            dataGridView1.Rows.Add();
            dataGridView1.Rows.Add();
            dataGridView1.Rows.Add();
            dataGridView1.Rows.Add();
            dataGridView1.Rows.Add();
            dataGridView1.Rows[0].Cells[0].Value = "1";
            dataGridView1.Rows[0].Cells[1].Value = "2";
            dataGridView1.Rows[0].Cells[2].Value = "8";
            dataGridView1.Rows[0].Cells[3].Value = "8";
            dataGridView1.Rows[0].Cells[4].Value = "220";
            dataGridView1.Rows[0].Cells[5].Value = "280";

            dataGridView1.Rows[1].Cells[0].Value = "1";
            dataGridView1.Rows[1].Cells[1].Value = "4";
            dataGridView1.Rows[1].Cells[2].Value = "10";
            dataGridView1.Rows[1].Cells[3].Value = "5";
            dataGridView1.Rows[1].Cells[4].Value = "100";
            dataGridView1.Rows[1].Cells[5].Value = "150";

            dataGridView1.Rows[2].Cells[0].Value = "2";
            dataGridView1.Rows[2].Cells[1].Value = "3";
            dataGridView1.Rows[2].Cells[2].Value = "6";
            dataGridView1.Rows[2].Cells[3].Value = "4";
            dataGridView1.Rows[2].Cells[4].Value = "300";
            dataGridView1.Rows[2].Cells[5].Value = "400";

            dataGridView1.Rows[3].Cells[0].Value = "3";
            dataGridView1.Rows[3].Cells[1].Value = "6";
            dataGridView1.Rows[3].Cells[2].Value = "12";
            dataGridView1.Rows[3].Cells[3].Value = "10";
            dataGridView1.Rows[3].Cells[4].Value = "260";
            dataGridView1.Rows[3].Cells[5].Value = "300";

            dataGridView1.Rows[4].Cells[0].Value = "4";
            dataGridView1.Rows[4].Cells[1].Value = "5";
            dataGridView1.Rows[4].Cells[2].Value = "15";
            dataGridView1.Rows[4].Cells[3].Value = "15";
            dataGridView1.Rows[4].Cells[4].Value = "150";
            dataGridView1.Rows[4].Cells[5].Value = "150";

            dataGridView1.Rows[5].Cells[0].Value = "5";
            dataGridView1.Rows[5].Cells[1].Value = "6";
            dataGridView1.Rows[5].Cells[2].Value = "10";
            dataGridView1.Rows[5].Cells[3].Value = "2";
            dataGridView1.Rows[5].Cells[4].Value = "200";
            dataGridView1.Rows[5].Cells[5].Value = "360";

            CreateTestGraph2();
            CreateTestGraph1();
        }

        void CreateTestGraph1()
        {
            Activities.Add(new Activity("1", "2", 8, 8, 0, 0));
            Activities.Add(new Activity("1", "4", 10, 5, 100, 150));
            Activities.Add(new Activity("4", "5", 15, 15, 0, 0));
            Activities.Add(new Activity("5", "6", 10, 2, 200, 360));
            Activities.Add(new Activity("2", "3", 6, 4, 300, 400));
            Activities.Add(new Activity("3", "6", 12, 10, 260, 300));
            Activities.Add(new Activity("6", "", 0, 0, 0, 0));
        }

        void CreateTestGraph2()
        {
            Activities.Add(new Activity("1", "2", 6, 4, 10, 16));
            Activities.Add(new Activity("2", "3", 4, 3, 13, 15));
            Activities.Add(new Activity("3", "4", 5, 5, 0, 0));
            Activities.Add(new Activity("4", "6", 10, 10, 0, 0));
            Activities.Add(new Activity("6", "7", 3, 2, 4, 8));
            Activities.Add(new Activity("3", "5", 8, 6, 16, 18));
            Activities.Add(new Activity("5", "7", 6, 6, 0, 0));
            Activities.Add(new Activity("7", "8", 2, 2, 0, 0));
            Activities.Add(new Activity("8", "", 0, 0, 0, 0));
        }
        private void RowsTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //allows backspace key
            if (e.KeyChar != '\b')
            {
                //allows just digits
                e.Handled = !char.IsNumber(e.KeyChar);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!isGraphCreated)
            {
                MessageBox.Show("ERROR", "Graph is not created!!!", MessageBoxButtons.OK);
                return;
            }

            (var paths, var cost) = GraphCPM.FindCriticalPaths(graph);

            bool ret = GraphCPM.Optimalize(ref graph, paths, cost, out double optimalizationCost);

            if (ret == false)
                LogLabel.Text = "Log: Graph cannot be more optimalized";
            if (optimalizationCost > -1)
            {
                CostsListLabel.Text += optimalizationCost.ToString() + "|";
                totalCost += optimalizationCost;
                TotalCostLabel.Text = "Total cost = " + totalCost.ToString();
            }

            gViewer.Graph = graph;
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            int[] onlyDightsColumns = { 2, 3, 4, 5 };
            e.Control.KeyPress -= new KeyPressEventHandler(Column2_KeyPress);
            if (onlyDightsColumns.Contains(dataGridView1.CurrentCell.ColumnIndex)) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column2_KeyPress);
                }
            }
        }

        private void Column2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            Activities = new List<Activity>();
            for(int i = 0; i < dataGridView1.RowCount-1 && dataGridView1.Rows[i] != null; i++)
            {
                string name = dataGridView1.Rows[i].Cells[(int)ActivityEnum.Name].Value?.ToString();
                string followingActivity= dataGridView1.Rows[i].Cells[(int)ActivityEnum.FollowingActivity].Value?.ToString();

                // do not add activity without name and following
                if (name == null || followingActivity == null)
                    continue;

                double normalTime = Convert.ToDouble(dataGridView1.Rows[i].Cells[(int)ActivityEnum.NormalTime].Value);
                double criticalTime = Convert.ToDouble(dataGridView1.Rows[i].Cells[(int)ActivityEnum.CriticalTime].Value);
                double normalCost = Convert.ToDouble(dataGridView1.Rows[i].Cells[(int)ActivityEnum.NormalCost].Value);
                double criticalCost = Convert.ToDouble(dataGridView1.Rows[i].Cells[(int)ActivityEnum.CriticalCost].Value);
                string description = dataGridView1.Rows[i].Cells[(int)ActivityEnum.Description].Value?.ToString() ?? "";
                Activities.Add(new Activity(name, followingActivity, normalTime, criticalTime, normalCost, criticalCost, description));
                Names.Add(name);
                Followings.Add(followingActivity);
            }

            for(int i = 0; i < Followings.Count; i++)
                if(!Names.Contains(Followings[i]))
                    Activities.Add(new Activity(Followings[i], "", 0, 0, 0, 0, ""));


            GraphCPM.CreateGraph(ref graph, Activities);
            Debug.WriteLine(graph.Nodes.Count());
            gViewer.Graph = graph;

            isGraphCreated = true;
            totalCost = 0;
            TotalCostLabel.Text = "Total cost = ";
            CostsListLabel.Text = "Costs: ";
            LogLabel.Text = "Log: ---";

            GraphCPM.AllGradients = new List<(string source, string target, double value)>();
            GraphCPM.CalculateAllGradients(ref graph);
        }
    }
}
