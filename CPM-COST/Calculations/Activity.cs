﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPM_COST.Calculations
{
    public class Activity
    {
        public Activity(string name, string followingActivityName, double normalTime = 0, double criticalTime = 0, double normalCost = 0, double criticalCost = 0, string description = "")
        {
            Name = name;
            FollowingActivityName = followingActivityName;
            NormalTime = normalTime;
            CriticalTime = criticalTime;
            NormalCost = normalCost;
            CriticalCost = criticalCost;
            Description = description;
        }

        public string Name { get; set; }
        public string FollowingActivityName { get; set; }
        public double NormalTime { get; set; }
        public double CriticalTime { get; set; }
        public double NormalCost { get; set; }
        public double CriticalCost { get; set; }
        public string Description { get; set; }
        
        public override string ToString() { return NormalTime.ToString() + "/" + CriticalTime.ToString(); }
    }
}
