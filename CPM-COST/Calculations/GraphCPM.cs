﻿using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Msagl.GraphViewerGdi;
using Microsoft.Msagl.Drawing;
using System.Linq;
using System;
using System.Diagnostics;

namespace CPM_COST.Calculations
{
    public static class GraphCPM
    {
        public static List<(string source, string target, double value)> AllGradients { get; set; } = new List<(string source, string target, double value)>();

        public static void CreateGraph(ref Graph graph, List<Activity> activities)
        {
            graph = new Graph();
            // Adding nodes
            foreach (Activity activity in activities)
                graph.AddNode(activity.Name);

            // Adding edges
            foreach (Activity activity in activities)
            {
                if(activity.FollowingActivityName != null && activity.FollowingActivityName != "")
                {
                    Edge e = new Edge(graph.FindNode(activity.Name), graph.FindNode(activity.FollowingActivityName), ConnectionToGraph.Connected);
                    e.UserData = activity;
                    e.LabelText = activity.ToString();
                }
            }
        }

        public static (List<List<Node>> longestPaths, List<double> costs) FindCriticalPaths(Graph graph)
        {
            Node start = null;
            Node end = null;

            // finding starting and ending point
            foreach (Node node in graph.Nodes)
            {
                if(node.InEdges.Count() == 0)
                    start = node;
                else if (node.OutEdges.Count() == 0)
                    end = node;

                if (start != null && end != null)
                    break;
            }

            return TraverseGraph(start, 0.0, new List<Node>(), 0, end);
        }

        private static (List<List<Node>> longestPaths, List<double> costs) TraverseGraph(Node currentNode, double pathLength, List<Node> path, double weight, Node end)
        {
            List<Node> ownPath = new List<Node>(path);
            ownPath.Add(currentNode);
            double ownPathLength = pathLength + weight;

            for (int i = 0; i < path.Count; i++)
            {
                if (currentNode == end) // path completed
                {
                    List<List<Node>> pathsList = new List<List<Node>>();
                    pathsList.Add(ownPath);
                    List<double> costs = new List<double>();
                    costs.Add(ownPathLength);
                    return (pathsList, costs);
                }
                else if (currentNode == path[i])
                {
                    return (null, null);
                }
            }

            ///DEBUG
            ///
            Console.WriteLine("Vertex no.: " + currentNode.Id);
            Console.WriteLine("Current path's length: " + ownPathLength);
            Console.Write("Visited: ");
            foreach (Node node in path)
            {
                Console.Write(node.Id + " ");
            }
            Console.WriteLine();
            ///

            List<List<Node>> allPaths = new List<List<Node>>();
            List<double> allCosts = new List<double>();
            for (int i = 0; i < currentNode.OutEdges.Count(); i++)
            {
                Node nextNode = currentNode.OutEdges.ElementAt(i).TargetNode;
                double nextWeight = ((Activity)(currentNode.OutEdges.ElementAt(i).UserData)).NormalTime;
                Console.WriteLine("Going " + currentNode.Id + "-> " + nextNode.Id);
                List<double> costs;
                List<List<Node>> paths;
                (paths, costs) = TraverseGraph(nextNode, ownPathLength, ownPath, nextWeight, end);

                if (paths != null && costs != null)
                {
                    allPaths.AddRange(paths);
                    allCosts.AddRange(costs);
                }
            }

            if (allPaths.Count == 0 || allCosts.Count == 0)
            {
                return (null, null);
            }

            List<double> longestCosts = new List<double>();
            List<List<Node>> longestPaths = new List<List<Node>>();

            List<double> sortedCosts = new List<double>(allCosts);
            sortedCosts.Sort();
            sortedCosts.Reverse();
            double longestPathsLength = sortedCosts[0];

            // add critical paths
            for (int i = 0; i < allPaths.Count; i++)
            {
                if (allCosts[i] == longestPathsLength)
                {
                    longestPaths.Add(allPaths[i]);
                    longestCosts.Add(allCosts[i]);
                }
            }

            if (sortedCosts.Count > 1)
            {
                // add one almost-critical cost
                double almostCriticalCost = -1;
                for (int i = 0; i < sortedCosts.Count; i++)
                {
                    if (sortedCosts[i] != longestPathsLength)
                    {
                        almostCriticalCost = sortedCosts[i];
                        longestCosts.Add(almostCriticalCost);
                        break; // need just one, for comparison in later algorithms
                    }
                }

                if (almostCriticalCost > -1)
                {
                    for (int i = 0; i < allCosts.Count; i++)
                    {
                        if (allCosts[i] == almostCriticalCost)
                        {
                            longestPaths.Add(allPaths[i]);
                            break;
                        }
                    }
                }
            }

            return (longestPaths, longestCosts);
        }

        public static void CalculateAllGradients(ref Graph graph)
        {
            for(int i = 0; i < graph.EdgeCount; i++)
            {
                Activity act = (Activity)graph.Edges.ElementAt(i).UserData;
                // default value
                double gradient = -1;

                // if it is possible to calculate
                if (act.NormalTime != act.CriticalTime)
                    gradient = (act.CriticalCost - act.NormalCost) / (act.NormalTime - act.CriticalTime);

                (string source, string target, double value) item = (act.Name, act.FollowingActivityName, gradient);

                // add to matrix
                AllGradients.Add(item);
            }
        }

        /// <summary>
        /// Function that optimalize graph using CPM-COST
        /// </summary>
        /// <param name="graph">Graph to optimalize</param>
        /// <param name="longestPaths">Return from FindCriticalPaths method</param>
        /// <param name="costs">Return from FindCriticalPaths method</param>
        /// <returns>True if was optimalized, false otherwise</returns>
        public static bool Optimalize(ref Graph graph, List<List<Node>> longestPaths, List<double> costs, out double optimalizationCost)
        {
            optimalizationCost = -1;
            // Checking count of critical parhs in longestPaths
            int criticalPathsCount = 1;

            if(costs.Count > 1)
                for (int i = 0; i < costs.Count - 1; i++)
                    if (costs[i] == costs[i + 1])
                        criticalPathsCount++;


            // check if optimalisation is possible
            bool isOptimalizable = false;
            for(int i = 0; i < criticalPathsCount; i++)
            {
                for(int j = 0; j<longestPaths[i].Count - 1; j++)
                {
                    Edge edge = longestPaths[i][j].OutEdges.ToArray().First(e => e.Target == longestPaths[i][j + 1].Id);
                    Activity activity = (Activity)edge.UserData;
                    if (activity.NormalTime != activity.CriticalTime)
                        isOptimalizable = true;
                }
            }
            if (isOptimalizable == false)
                return false;

            // create gradient matrix
            List<List<double>> gradients = new List<List<double>>();
            for (int i = 0; i < criticalPathsCount; i++)
                gradients.Add(new List<double>());

            // calculate gradient matrix
            // for every critical path
            for(int i = 0; i < criticalPathsCount; i++)
            {
                // for every edge
                for(int j =0; j< longestPaths[i].Count - 1; j++)
                {
                    Edge edge = longestPaths[i][j].OutEdges.ToArray().FirstOrDefault(e => e.Target == longestPaths[i][j + 1].Id);
                    string source = edge.Source;
                    string target = edge.Target;

                    (string source, string target, double value) grad = AllGradients.Where(g => (g.source == source && g.target == target)).ElementAt(0);

                    Activity act = (Activity)edge.UserData;

                    if (act.NormalTime != act.CriticalTime)
                        gradients[i].Add(grad.value);
                    else
                        gradients[i].Add(-1);
                }
            }

            // difference between longest and almost longest
            double pathCostDiff = 0;
            if (criticalPathsCount < longestPaths.Count)
                pathCostDiff = costs[criticalPathsCount - 1] - costs[criticalPathsCount];
            else
                pathCostDiff = double.MaxValue;     // if there is no shorter path set diff as MaxVal

            // find indexes of the smalest gradients
            List<int> smallestGradientsIndexes = new List<int>(new int[gradients.Count]);
            for (int i = 0; i < smallestGradientsIndexes.Count; i++)
                smallestGradientsIndexes[i] = -1;

            for (int i = 0; i < gradients.Count; i++)           // for every critical path
            {
                for(int j = 0; j < gradients[i].Count; j++)     // for every conection
                {
                    // if smalest is not initialized
                    if(smallestGradientsIndexes[i] == -1 && gradients[i][j] >= 0)
                        smallestGradientsIndexes[i] = j;

                    // if found smalest then currnet
                    if (gradients[i][j] != -1 && gradients[i][j] < gradients[i][smallestGradientsIndexes[i]])
                        smallestGradientsIndexes[i] = j;
                }
            }


            // find activities to optimalization;
            List<(Node, Node)> optNodes = new List<(Node, Node)>(criticalPathsCount);
            // find value of optimalization
            double optVal = -1;
            for(int i = 0; i< criticalPathsCount; i++)
            {
                // Activities on critical paths with smalest gradients
                Node a = longestPaths[i][smallestGradientsIndexes[i]];
                Node b = longestPaths[i][smallestGradientsIndexes[i] + 1];

                optNodes.Add((a, b));

                // select activity
                Edge edge = a.OutEdges.ToArray().FirstOrDefault(e => e.Target == b.Id);
                Activity act = ((Activity)edge.UserData);

                // find smalest dT to optimalization on every path
                if (optVal == -1)
                    optVal = act.NormalTime - act.CriticalTime;
                else if (optVal != -1 && optVal > act.NormalTime - act.CriticalTime)
                    optVal = act.NormalTime - act.CriticalTime;

            }

            // final dT is smalest value between optVal and pathCostDiff
            optVal = optVal < pathCostDiff ? optVal : pathCostDiff;
            for (int i = 0; i< optNodes.Count; i++)
            {
                Node a = graph.Nodes.First(n => n.Id == optNodes[i].Item1.Id);
                Node b = graph.Nodes.First(n => n.Id == optNodes[i].Item2.Id);
                Edge edge = a.OutEdges.ToArray().FirstOrDefault(e => e.Target == b.Id);


                Activity act = (Activity)edge.UserData;
                act.NormalTime -= optVal;
                edge.LabelText = act.ToString();

                Debug.WriteLine(act.Name + "---" + act.FollowingActivityName + ":   " + optVal);
            }

            double optCost = 0;
            for (int i =0; i< criticalPathsCount; i++)
            {
                double sfsdf = gradients[i][smallestGradientsIndexes[i]];
                optCost += optVal * gradients[i][smallestGradientsIndexes[i]];
            }
            optimalizationCost = optCost;

            Debug.WriteLine("---------------------------");


            return true;
        }
    }
}
