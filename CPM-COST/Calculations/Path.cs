﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPM_COST.Calculations
{
    public class Path
    {
        public List<string> Nodes { get; set; }
        public double NormalCost { get; set; } = 0;
        public double CriticalCost { get; set; } = 0;
    }
}
