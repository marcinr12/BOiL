﻿namespace TransportationProblem.Calculations
{
    public class Edge
    {
        public int Item1 { get; set; }
        public int Item2 { get; set; }
        public int Weight { get; set; }
        public Edge(int i1, int i2, int w)
        {
            Item1 = i1;
            Item2 = i2;
            Weight = w;
        }
    }
}
