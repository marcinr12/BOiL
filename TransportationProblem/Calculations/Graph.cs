using System;
using System.Collections.Generic;

namespace TransportationProblem.Calculations
{
    public class Graph
    {
        public Dictionary<int, Cell> AdjacencyList { get; } = new Dictionary<int, Cell>();
        private int id = 0;
        public Graph() { }
        public Graph(IEnumerable<Coordinates> coordinates, IEnumerable<Edge> edges)
        {
            foreach (var coordinate in coordinates)
            AddVertex(coordinate);

            foreach (var edge in edges)
            AddEdge(edge);
        }        

        public void AddVertex(Coordinates coordinates)
        {
            AdjacencyList[id] = new Cell(coordinates);
            id++;
        }

        public void AddEdge(Edge edge)
        {
            if (AdjacencyList.ContainsKey(edge.Item1) && AdjacencyList.ContainsKey(edge.Item2))
            {
                AdjacencyList[edge.Item1].Edges.Add(new Tuple<int, int>(edge.Item2, edge.Weight));
                AdjacencyList[edge.Item2].Edges.Add(new Tuple<int, int>(edge.Item1, edge.Weight));
            }
        }
    }
}
