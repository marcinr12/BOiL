﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TransportationProblem.Calculations
{
    public static class Calculations
    {
        private static List<List<double>> VariableMatrix { get; set; }
        private static List<double> CostsOfBaseCells { get; set; }
        private static List<List<double>> TransportWares { get; set; }
        private static List<List<double>> TransportCosts { get; set; }
        private static List<double> Alphas { get; set; }
        private static List<double> Betas { get; set; }
        private static List<List<double>> OptimalizationCoefficients { get; set;}
        private static List<(int x, int y)> CycleCoordinates { get; set; }

        public static void Print2DList<T>(List<List<T>> list)
        {
            foreach (var l in list)
                Print1DList(l);
        }

        public static void Print1DList<T>(List<T> list)
        {
            foreach (var l in list)
                Console.Write(l + "   ");
            Console.WriteLine();
        }

        public static (int x, int y) FindMinRemainingIndex(List<double> dos, List<double> odb, List<List<double>> koszt)
        {
            int x = -1;
            int y = -1;
            double min = Double.MaxValue;

            for (int i = 0; i < dos.Count; i++)
            {
                if (dos[i] == 0)
                    continue;
                for (int j = 0; j < odb.Count; j++)
                {
                    if (odb[j] == 0)
                        continue;
                    if (koszt[i][j] < min)
                    {
                        min = koszt[i][j];
                        x = i;
                        y = j;
                    }
                }
            }
            return (x, y);
        }

        public static List<List<double>> AsignInitialWares(List<double> dos, List<double> odb, List<List<double>> koszt)
        {
            // inicjalizacja macierzy zerami
            List<List<double>> list = new List<List<double>>();
            foreach (var l in koszt)
            {
                list.Add(new List<double>(new double[l.Count]));
            }

            (int x, int y) indexes = FindMinRemainingIndex(dos, odb, koszt);
            while (indexes.x != -1 && indexes.y != -1)
            {
                if (dos[indexes.x] < odb[indexes.y])
                {
                    list[indexes.x][indexes.y] += dos[indexes.x];
                    odb[indexes.y] -= dos[indexes.x];
                    dos[indexes.x] = 0;
                }
                else
                {
                    list[indexes.x][indexes.y] += odb[indexes.y];
                    dos[indexes.x] -= odb[indexes.y];
                    odb[indexes.y] = 0;
                }
                indexes = FindMinRemainingIndex(dos, odb, koszt);
            }

            Calculations.TransportWares = list;

            return TransportWares;
        }

        public static double TotalCost(List<List<double>> koszty)
        {
            double totalCost = 0;
            for (int i = 0; i < koszty.Count; i++)
                for (int j = 0; j < koszty[i].Count; j++)
                    totalCost += koszty[i][j] * TransportWares[i][j];
            return totalCost;
        }

        public static (List<double> Alphas, List<double> Betas) CalculateDualVariables(int rows, int columns, List<List<double>> TransportCosts)
        {
            Calculations.TransportCosts = TransportCosts;

            VariableMatrix = new List<List<double>>();
            VariableMatrix.Add(new List<double>(new double[(int)(rows + columns)]));

            CostsOfBaseCells = new List<double>();
            CostsOfBaseCells.Add(0); // alpha_1 equals to 0
            VariableMatrix[0][0] = 1;

            int k = 1;
            for (int i = 0; i < TransportWares.Count; i++)
            {
                for (int j = 0; j < TransportWares[i].Count; j++)
                {
                    if (TransportWares[i][j] != 0)
                    {
                        VariableMatrix.Add(new List<double>(new double[(int)(rows + columns)]));
                        VariableMatrix[k][i] += 1;
                        VariableMatrix[k][j + rows] += 1;
                        k++;

                        CostsOfBaseCells.Add(TransportCosts[i][j]); // additional matrix
                    }
                }
            }

            Console.WriteLine("Variable matrix");
            Print2DList(VariableMatrix);

            Console.WriteLine("\nCostsOfBaseCells");
            Print1DList(CostsOfBaseCells);
            Console.WriteLine("_________________________________________");

            // calculating vector with alphas and betas
            Matrix<double> A = Matrix<double>.Build.DenseOfArray(JaggedToArray2D(VariableMatrix));
            Vector<double> b = Vector<double>.Build.Dense(CostsOfBaseCells.ToArray());
            Vector<double> x = A.Solve(b);

            // adding to list of alphas
            k = 0;
            Alphas = new List<double>();
            while(k < rows)
            {
                Alphas.Add(x[k]);
                k++;
            }
            // adding to list of betas
            Betas = new List<double>();
            while (k < columns + rows)
            {
                Betas.Add(x[k]);
                k++;
            }

            Console.Write("Alfy: ");        Print1DList(Alphas);
            Console.Write("Bety: ");        Print1DList(Betas);

            // round to like 4 positions?
            return (Alphas, Betas);
        }

        public  static T[,] JaggedToArray2D<T>(List<List<T>> source)
        {
            T[,] result = new T[source.Count, source[0].Count];

            for (int i = 0; i < source.Count; i++)
            {
                for (int k = 0; k < source[i].Count; k++)
                {
                    result[i, k] = source[i][k];
                }
            }

            return result;
        }

        public static List<List<double>> CalculateOptimalizationCoefficients(int rows, int columns)
        {
            // inicjalizacja macierzy zerami
            OptimalizationCoefficients = new List<List<double>>();
            for(int i = 0; i < rows; i++)
                OptimalizationCoefficients.Add(new List<double>(new double[columns]));

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if(TransportWares[i][j] != 0)
                    {
                        OptimalizationCoefficients[i][j] = 0;
                    }
                    else
                    {
                        OptimalizationCoefficients[i][j] = TransportCosts[i][j] - Alphas[i] - Betas[j];
                    }
                }
            }

            Print2DList(OptimalizationCoefficients);

            return OptimalizationCoefficients;
        }

        public static List<(int x, int y)> FindChangeCircuit()
        {
            CycleCoordinates = new List<(int x, int y)>();

            List<Coordinates> coordinates = new List<Coordinates>();
            List<Edge> edges = new List<Edge>();
            var smallest = (value: 0.0, position: -1);

            int rows = OptimalizationCoefficients.Count;
            int columns = OptimalizationCoefficients[0].Count;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (OptimalizationCoefficients[i][j] == 0)
                    {
                        coordinates.Add(new Coordinates(j, i)); // i goes vertically -> y
                    }
                    else if (OptimalizationCoefficients[i][j] < 0 && OptimalizationCoefficients[i][j] < smallest.value)
                    {
                        if(smallest.position == -1)
                        {
                            coordinates.Add(new Coordinates(j, i));
                            smallest.position = coordinates.Count - 1;
                            smallest.value = OptimalizationCoefficients[i][j];
                        }
                        else
                        {
                            coordinates[smallest.position] = new Coordinates(j, i);
                            smallest.value = OptimalizationCoefficients[i][j];
                        }
                        //coordinates.Add(new Coordinates(j, i)); // i goes vertically -> y
                        int inff = 1;
                    }
                }
            }

            for (int id = 0; id < coordinates.Count - 1; id++)
            {
                Coordinates c = coordinates[id];

                for (int id2 = id + 1; id2 < coordinates.Count; id2++)
                {
                    Coordinates c2 = coordinates[id2];
                    if(c.X == c2.X)
                    {
                        int weight = 1;
                        edges.Add(new Edge(id, id2, weight));
                    }
                    else if(c.Y == c2.Y)
                    {
                        int weight = -1;
                        edges.Add(new Edge(id, id2, weight));
                    }
                }
            }

            Graph graph = new Graph(coordinates, edges);

            bool cycleFound = false;
            List<int> cycle;

            if (smallest.position == -1)
                return CycleCoordinates;

            (cycleFound, cycle) = TraverseGraph(graph, smallest.position, 0, new List<int>(), 0, smallest.position);

            if(cycleFound == true)
            {
                for(int i = 0; i < cycle.Count; i++)
                {
                    Coordinates cellCoordinates = graph.AdjacencyList[cycle[i]].Coordinates;
                    CycleCoordinates.Add((cellCoordinates.X, cellCoordinates.Y));
                }
            }

            return CycleCoordinates;
        }

        private static (bool exists, List<int> shortestPath) TraverseGraph(Graph graph, int vertex, int pathLength, List<int> path, int weight, int end)
        {
            for (int i = 0; i < path.Count; i++)
            {
                if(vertex == end) // cycle completed
                {
                    return (true, path);
                }
                else if (vertex == path[i])
                {
                    return (false, null);
                }
            }

            List<int> ownPath = new List<int>(path);
            ownPath.Add(vertex);
            int ownPathLength = pathLength + 1;

            ///DEBUG
            ///
            Console.WriteLine("Vertex no.: " + vertex);
            Console.WriteLine("Current path's length: " + pathLength);
            Console.Write("Visited: ");
            foreach (int visited in path)
            {
                Console.Write(visited + " ");
            }
            Console.WriteLine();
            ///

            Cell cell = graph.AdjacencyList[vertex];
            List<List<int>> cycles = new List<List<int>>();
            if (weight == 0) // simplified for - can go in any direction from starting point
            {
                for (int i = 0; i < cell.Edges.Count; i++)
                {
                    Tuple<int, int> neighborVertex = cell.Edges.ElementAt(i);
                    int nextWeight = neighborVertex.Item2;

                    Console.WriteLine("Going " + vertex + "-> " + neighborVertex.Item1);
                    bool isCycle;
                    List<int> cycle;
                    (isCycle, cycle) = TraverseGraph(graph, neighborVertex.Item1, ownPathLength, ownPath, nextWeight, end);
                    if(isCycle == true)
                    {
                        cycles.Add(cycle);
                    }
                }
            }
            else // for with constraints - cannot move in the same direction twice
            {
                for (int i = 0; i < cell.Edges.Count; i++)
                {
                    Tuple<int, int> neighborVertex = cell.Edges.ElementAt(i);
                    int nextWeight = neighborVertex.Item2;

                    if (weight + neighborVertex.Item2 == 0)
                    {
                        Console.WriteLine("Going " + vertex + "-> " + neighborVertex.Item1);
                        bool isCycle;
                        List<int> cycle;
                        (isCycle, cycle) = TraverseGraph(graph, neighborVertex.Item1, ownPathLength, ownPath, nextWeight, end);
                        if (isCycle == true)
                        {
                            cycles.Add(cycle);
                        }
                    }
                    else continue;
                }
            }

            if(cycles.Count == 0)
            {
                return (false, null);
            }

            int shortestPathLength = cycles[0].Count;
            List<int> shortestPath = new List<int>(cycles[0]);
            for (int i = 0; i < cycles.Count; i++)
            {
                if(cycles[i].Count < shortestPathLength)
                {
                    shortestPathLength = cycles[i].Count;
                    shortestPath = new List<int>(cycles[i]);
                }
            }

            return (true, shortestPath);
        }

        public static void ChangeTransportWares()
        {
            double change = -1; // TODO: cleverer idea of handling this
            for (int i = 1; i < CycleCoordinates.Count; i = i + 2) // itearating through even elements of a cycle - substracting
            {
                double currentVal = TransportWares[CycleCoordinates[i].y][CycleCoordinates[i].x];
                if (change == -1 || currentVal < change)
                {
                    change = currentVal;
                }
            }

            // adjusting TransportCosts
            for (int i = 0; i < CycleCoordinates.Count; i = i + 2) // adding
            {
                TransportWares[CycleCoordinates[i].y][CycleCoordinates[i].x] += change;
            }
            for (int i = 1; i < CycleCoordinates.Count; i = i + 2) // substracting
            {
                TransportWares[CycleCoordinates[i].y][CycleCoordinates[i].x] -= change;
            }
        }
    }
}
