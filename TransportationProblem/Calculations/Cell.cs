using System;
using System.Collections.Generic;

namespace TransportationProblem.Calculations
{
    public class Cell
    {
        public Coordinates Coordinates {get; set;}
        public HashSet<Tuple<int, int>> Edges {get; set;} // <neighbor's vertex, weight>

        public Cell(Coordinates coordinates)
        {
            this.Coordinates = coordinates;
            this.Edges = new HashSet<Tuple<int, int>>();
        }
    }
}