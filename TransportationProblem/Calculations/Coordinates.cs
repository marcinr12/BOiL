namespace TransportationProblem.Calculations
{
    public class Coordinates
    {
        public int X {get; set;}
        public int Y {get; set;}

        private Coordinates() { }
        public Coordinates(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }

}