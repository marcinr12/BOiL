using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Cal = TransportationProblem.Calculations.Calculations;

namespace TransportationProblem
{
    public partial class Form1 : Form
    {
        private List<List<double>> TransportCosts;
        private List<List<double>> TransportWares;
        private List<double> Alphas;
        private List<double> Betas;
        private List<double> Odbiorcy;
        private List<double> Dostawcy;
        private String DateString;
        private Double TotalCost;
        public int Rows { get; set; } = 3;
        public int Columns { get; set; } = 3;
        private bool IsFirstIteration { get; set; } = true;
        private bool IsOptimalSolution { get; set; } = false;
        public Form1()
        { 
            InitializeComponent();

            DateString = DateTime.Now.ToString("ddMMyyyyHHmmssfff");

            CreateDataGridView(Rows, Columns);
            // TEST CASE
            dataGridView1.Rows[0].Cells[0].Value = 2;
            dataGridView1.Rows[0].Cells[1].Value = 5;
            dataGridView1.Rows[0].Cells[2].Value = 4;
            dataGridView1.Rows[1].Cells[0].Value = 1;
            dataGridView1.Rows[1].Cells[1].Value = 3;
            dataGridView1.Rows[1].Cells[2].Value = 6;
            dataGridView1.Rows[2].Cells[0].Value = 2;
            dataGridView1.Rows[2].Cells[1].Value = 2;
            dataGridView1.Rows[2].Cells[2].Value = 7;

            dataGridView1.Rows[0].Cells[3].Value = 20;
            dataGridView1.Rows[1].Cells[3].Value = 30;
            dataGridView1.Rows[2].Cells[3].Value = 20;

            dataGridView1.Rows[3].Cells[0].Value = 25;
            dataGridView1.Rows[3].Cells[1].Value = 28;
            dataGridView1.Rows[3].Cells[2].Value = 17;
            //


            CreateListsFromDataGridView();
            //DisplayListDebug();            
        }

        private void CreateDataGridView(int rows, int columns)
        {
            dataGridView1.Columns.Clear();
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;

            // Dodanie kolumn
            for (int i = 0; i < columns; i++)
            {
                dataGridView1.Columns.Add("O" + (i + 1), "O" + (i + 1));
                dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[i].Width = 60;
            }
            dataGridView1.Columns.Add("Podaż", "Podaż");
            dataGridView1.Columns[columns].SortMode = DataGridViewColumnSortMode.NotSortable;
            dataGridView1.Columns[columns].Width = 60;

            // dodanie wierszy
            for (int i = 0; i < rows; i++)
            {
                dataGridView1.Rows.Insert(i);
                dataGridView1.Rows[i].HeaderCell.Value = "D" + (i + 1);
            }
            dataGridView1.Rows.Insert(rows);
            dataGridView1.Rows[rows].HeaderCell.Value = "Popyt";

            // ustawianie domyślnej wartości
            foreach (DataGridViewRow row in dataGridView1.Rows)
                foreach (DataGridViewCell cell in row.Cells)
                    cell.Value = "0";

            // ustawienie szerokosci RowHeaders
            dataGridView1.RowHeadersWidth = 80;
        }

        public void CreateListsFromDataGridView()
        {
            this.TransportCosts = new List<List<double>>();
            // list.Count -> liczba kolumn
            // list[0].Count -> liczba wierszy

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                this.TransportCosts.Add(new List<double>());
                for (int j = 0; j < dataGridView1.ColumnCount - 1; j++)
                {
                    this.TransportCosts[i].Add(Convert.ToDouble(dataGridView1[j, i].Value));
                }
            }

            Dostawcy = new List<double>();
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                Dostawcy.Add(Convert.ToDouble(dataGridView1[Columns, i].Value));

            Odbiorcy = new List<double>();
            for (int i = 0; i < dataGridView1.ColumnCount - 1; i++)
                Odbiorcy.Add(Convert.ToDouble(dataGridView1[i, Rows].Value));
        }

        public void DisplayListDebug()
        {
            for (int i = 0; i < this.TransportCosts.Count; i++)
            {
                for (int j = 0; j < this.TransportCosts[i].Count; j++)
                    Console.Write(this.TransportCosts[i][j] + "   ");
                Console.WriteLine();
            }
        }

        void DisplayIterationGrid()
        {
            if (TransportWares == null)
            {
                MessageBox.Show("It appears that the list containing amounts of wares to transport doesn't exist or is empty. " +
                    "Make sure to calculate the result first before displaying data.", "Null exception", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int rows = TransportWares.Count;

            if (rows == 0)
            {
                MessageBox.Show("It appears there are no rows defined for the problem. " +
                    "Make sure the grid is created properly", "Empty list", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int columns = TransportWares[0].Count;

            if (columns == 0)
            {
                MessageBox.Show("It appears there are no columns defined for the problem. " +
                    "Make sure the grid is created properly", "Empty list", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            dataGridView2.Columns.Clear();
            dataGridView2.AllowUserToAddRows = false;
            dataGridView2.AllowUserToDeleteRows = false;
            dataGridView2.AllowUserToResizeColumns = false;
            dataGridView2.AllowUserToResizeRows = false;
            dataGridView2.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;

            // Dodanie kolumn
            for (int i = 0; i < columns; i++)
            {
                dataGridView2.Columns.Add("O" + (i + 1), "O" + (i + 1));
                dataGridView2.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView2.Columns[i].Width = 60;
            }
            dataGridView2.Columns.Add("Alfy", "Alfy");
            dataGridView2.Columns[columns].SortMode = DataGridViewColumnSortMode.NotSortable;

            // dodanie wierszy
            for (int i = 0; i < rows; i++)
            {
                dataGridView2.Rows.Insert(i);
                dataGridView2.Rows[i].HeaderCell.Value = "D" + (i + 1);
            }
            dataGridView2.Rows.Insert(rows);
            dataGridView2.Rows[rows].HeaderCell.Value = "Bety";

            // set transport grid
            for (int i = 0; i < rows; i++)
            {
                DataGridViewRow row = dataGridView2.Rows[i];
                for (int j = 0; j < columns; j++)
                {
                    DataGridViewCell cell = row.Cells[j];
                    cell.Value = TransportWares[i][j];
                }
            }

            //set dual variables grid
            for(int i = 0; i < Alphas.Count; i++)
            {
                DataGridViewRow row = dataGridView2.Rows[i];
                DataGridViewCell cell = row.Cells[columns];
                cell.Value = Alphas[i];
            }

            for (int i = 0; i < Betas.Count; i++)
            {
                DataGridViewRow row = dataGridView2.Rows[rows];
                DataGridViewCell cell = row.Cells[i];
                cell.Value = Betas[i];
            }

            dataGridView2.RowHeadersWidth = 80;
        }

        void DisplayOptimalizationCoefficients(int rows, int columns)
        {
            List<List<double>> OptimalizationCoefficients = Cal.CalculateOptimalizationCoefficients(Rows, Columns);

            dataGridView3.Columns.Clear();
            dataGridView3.AllowUserToAddRows = false;
            dataGridView3.AllowUserToDeleteRows = false;
            dataGridView3.AllowUserToResizeColumns = false;
            dataGridView3.AllowUserToResizeRows = false;
            dataGridView3.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;

            // Dodanie kolumn
            for (int i = 0; i < columns; i++)
            {
                dataGridView3.Columns.Add("", "");
                dataGridView3.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView3.Columns[i].Width = 60;
            }
            dataGridView3.ColumnHeadersVisible = false;

            // Dodanie wierszy
            for (int i = 0; i < rows; i++)
            {
                dataGridView3.Rows.Insert(i);
            }
            dataGridView3.RowHeadersVisible = false;

            // Set optimalization coefficients grid
            for (int i = 0; i < rows; i++)
            {
                DataGridViewRow row = dataGridView3.Rows[i];
                for (int j = 0; j < columns; j++)
                {
                    DataGridViewCell cell = row.Cells[j];
                    cell.Value = OptimalizationCoefficients[i][j];
                }
            }

            dataGridView3.RowHeadersWidth = 80;
        }

        /// <summary>
        /// Returns true if found optimal solution
        /// </summary>
        /// <returns></returns>
        bool PaintCycle()
        {
            List<(int x, int y)> cycleCoordinates = Cal.FindChangeCircuit();

            if (cycleCoordinates.Count == 0)
                return true;

            for (int i = 0; i < cycleCoordinates.Count; i++)
            {
                int x = cycleCoordinates[i].x;
                int y = cycleCoordinates[i].y;
                DataGridViewCell cell = dataGridView3.Rows[y].Cells[x];

                cell.Style.BackColor = Color.Yellow;
                cell.Style.Font = new Font(dataGridView3.Font, FontStyle.Bold);
            }

            return false;
        }

        void AdjustTransportWares()
        {
            // TODO: if cycle < 4 or cycle odd - ERROR
            Cal.ChangeTransportWares();
        }

        private void DisplayButton_Click(object sender, EventArgs e)
        {
            DisplayListDebug();
            (Alphas, Betas) = Cal.CalculateDualVariables(Rows, Columns, TransportCosts);
            DisplayIterationGrid();
            DisplayOptimalizationCoefficients(Rows, Columns);
            PaintCycle();
            AdjustTransportWares();
            // testing only
            (Alphas, Betas) = Cal.CalculateDualVariables(Rows, Columns, TransportCosts); // TODO: getter for alphas and betas inside DisplayIteration...()
            DisplayIterationGrid();
            DisplayOptimalizationCoefficients(Rows, Columns);
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            IsFirstIteration = true;
            IsOptimalSolution = false;

            if (!Int32.TryParse(rowsTextBox.Text, out int rows))
            {
                errors.Text = "Errors: could not parse rows number.";
                return;
            }
            else
                errors.Text = "Errors: ---";
                
            if (!Int32.TryParse(columnsTextBox.Text, out int columns))
            {
                errors.Text = "Errors: could not parse columns number.";
                return;
            }
            else
                errors.Text = "Errors: ---";

            this.Rows = rows;
            this.Columns = columns;
            CreateDataGridView(rows, columns);
            CreateListsFromDataGridView();

            string basePath = System.AppDomain.CurrentDomain.BaseDirectory;
            string filePath = basePath + @"..\..\" + @"Saved Iterations\File_" + DateString + ".csv";
            string separator = "#####################\n";
            File.AppendAllText(filePath, separator);
        }

        private void DataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        }

        private void Calculate_Click(object sender, EventArgs e)
        {
            CreateListsFromDataGridView();

            if (IsFirstIteration == true)
                TransportWares = Cal.AsignInitialWares(this.Dostawcy, this.Odbiorcy, this.TransportCosts);

            while (IsOptimalSolution == false)
            {
                if (IsFirstIteration == false)
                    AdjustTransportWares();
                else
                    IsFirstIteration = false;


                (Alphas, Betas) = Cal.CalculateDualVariables(Rows, Columns, TransportCosts);
                DisplayIterationGrid();
                DisplayOptimalizationCoefficients(Rows, Columns);
                IsOptimalSolution = PaintCycle();

                // total cost calculation and display
                TotalCost = Cal.TotalCost(this.TransportCosts);
                labelTotalCost.Text = TotalCost.ToString();
                SaveToCSV();
            }
        }

        private void RowsTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //allows backspace key
            if (e.KeyChar != '\b')
            {
                //allows just digits
                e.Handled = !char.IsNumber(e.KeyChar);
            }
        }

        private void ColumnsTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //allows backspace key
            if (e.KeyChar != '\b')
            {
                //allows just digits
                e.Handled = !char.IsNumber(e.KeyChar);
            }
        }

        // single click to edit value in cell
        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > -1 && e.RowIndex > -1)
            {
                dataGridView1.CurrentCell = dataGridView1[e.ColumnIndex, e.RowIndex];
                dataGridView1.BeginEdit(true);
                dataGridView1.CurrentCell.Value = "";
                ((TextBox)dataGridView1.EditingControl).SelectionStart = dataGridView1.CurrentCell.Value.ToString().Length;
            }
        }

        private void OneIteration_Click(object sender, EventArgs e)
        {
            CreateListsFromDataGridView();

            if (IsFirstIteration == true)
                TransportWares = Cal.AsignInitialWares(this.Dostawcy, this.Odbiorcy, this.TransportCosts);

            if (IsOptimalSolution == false)
            {
                if (IsFirstIteration == false)
                    AdjustTransportWares();
                else
                    IsFirstIteration = false;


                (Alphas, Betas) = Cal.CalculateDualVariables(Rows, Columns, TransportCosts);
                DisplayIterationGrid();
                DisplayOptimalizationCoefficients(Rows, Columns);
                IsOptimalSolution = PaintCycle();

                // total cost calculation and display
                TotalCost = Cal.TotalCost(this.TransportCosts);
                labelTotalCost.Text = TotalCost.ToString();
                SaveToCSV();
            }            
        }

        private void SaveToCSV()
        {
            string basePath = System.AppDomain.CurrentDomain.BaseDirectory;
            string filePath = basePath + @"..\..\" + @"Saved Iterations\File_" + DateString + ".csv";

            int gapLength = 3;

            string line = "Transport Wares";
            line += new string(';', Columns + 1) + "Alphas";
            line += new string(';', gapLength + 1) + "Optimalization Coefficients\n";

            for (int i = 0; i < Rows + 1; i++)
            {
                DataGridViewRow row = dataGridView2.Rows[i];

                if (i == Rows) line += "Betas" + ";";
                else line += ";";

                for (int j = 0; j < Columns + 1; j++)
                {
                    DataGridViewCell cell = row.Cells[j];
                    line += cell.Value + ";";
                }

                if(i < Rows)
                {
                    line += new string(';', gapLength);
                    DataGridViewRow row3 = dataGridView3.Rows[i];
                    for(int j = 0; j < Columns; j++)
                    {
                        DataGridViewCell cell = row3.Cells[j];
                        line += cell.Value + ";";
                    }
                }                    
                line += "\n";
            }
            line += "Total cost for one iteration:" + ";";
            line += TotalCost + "\n";

            line += "\n\n\n";

            File.AppendAllText(filePath, line);
        }
    }    
}
